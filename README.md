This repository contains all the files and scripts necessary to generate provenance of tissue sample acquisition in hospital surgery department.

## Project structure

- `provenance`- contains provenance generation code
- `data` - contains input files
- `output` - contains output files


## Provenance generation
Once the populated anonymized record is available we can run the provenance generation by executing the following commands from the `prov_mmci` directory:

```bash
python -m provenance.bundles.sample_acquisition --nis_log data/LIMS_anon.json
python -m provenance.bundles.sample_processing --nis_log data/LIMS_anon.json
python -m provenance.bundles.metabundle --provn_dir output/provn_pid/provn/
```
where

- `--nis_log` refers to the anonymized HIS record
- `--provn_dir` refers to the directory where PROV-N files for sample_acquisition and sample_processing are located at

These scripts will generate PROV-N files as well as their graphical representation in PNG format.

## External requests
Connections to external organisations are handled via `prov_req.json` file. External organisation submits the file containg information about their backward connector entity. The information contained within the request are then used to create a forward connector in MMCI provenance. 

## Upload to DOI repository
Finally, we generate and upload PIDs to the DOI repository. In this example we are using Datacite service. To successfully upload the PID information **you will need your own registered organization DOI in the Datacite**. The scripts reads datacite credentials from environmental variables:

- `DATACITE_REPOSITORY_USERNAME`
- `DATACITE_REPOSITORY_PASSWORD`

You can then run the following command from the `prov_mmci` directory:

```bash
python -m provenance.bundles.generate_pids --json_dir output/provn_pid/json/
```

where
- `--json_dir` refers to the directory containing JSON representations of the generated PROV-N files. These are created during the previous step


