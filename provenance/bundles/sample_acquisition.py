# Built-in Imports
import argparse
import json
import uuid
from pathlib import Path

# Third-party Imports
import prov.model as prov
import pygit2

# Local Imports
from provenance.constants import (
    PROVN_NAMESPACE, 
    PROVN_NAMESPACE_URI,
    DOI_NAMESPACE,
    DOI_NAMESPACE_URI,
    DEFAULT_NAMESPACE_URI,
    NAMESPACE_COMMON_MODEL,
    NAMESPACE_COMMON_MODEL_URI,
    NAMESPACE_DCT,
    NAMESPACE_DCT_URI,
    NAMESPACE_PROV,
    BUNDLE_SURGERY_DEPARTMENT,
    BUNDLE_META,
    OUTPUT_DIR,
    GRAPH_NAMESPACE_URI
)
from provenance.utils import export_to_image, export_to_file
from provenance.utils import parse_log


def add_namespaces(prov_obj):
    # Declaring namespaces
    prov_obj.add_namespace(PROVN_NAMESPACE, PROVN_NAMESPACE_URI)
    prov_obj.add_namespace(DOI_NAMESPACE, DOI_NAMESPACE_URI)
    prov_obj.add_namespace(NAMESPACE_COMMON_MODEL, NAMESPACE_COMMON_MODEL_URI)
    prov_obj.add_namespace(NAMESPACE_DCT, NAMESPACE_DCT_URI)
    prov_obj.set_default_namespace(DEFAULT_NAMESPACE_URI)


def export_provenance(nis_log: Path, prov_requests: Path):
    doc = prov.ProvDocument()
    add_namespaces(doc)
    
    # Read provenance requests
    with prov_requests.open('r') as json_in:
        prov_req = json.load(json_in)

    # Creating NIS Sample Acquisition bundle
    bndl = doc.bundle(f"{PROVN_NAMESPACE}:{BUNDLE_SURGERY_DEPARTMENT}")
    add_namespaces(bndl)
    
    # Forward connector
    entity_identifier = 'sampleConnector'
    for prefix_name, prefix_uri in prov_req[entity_identifier]['prefixes'].items():
        doc.add_namespace(prefix_name, prefix_uri)
        bndl.add_namespace(prefix_name, prefix_uri)
    conn_sample_connector = bndl.entity(f"{DOI_NAMESPACE}:{entity_identifier}", other_attributes={
        f"{NAMESPACE_PROV}:type": bndl.valid_qualified_name(f'{NAMESPACE_COMMON_MODEL}:forwardConnector'),
        f"{NAMESPACE_COMMON_MODEL}:receiverBundleId": bndl.valid_qualified_name(prov_req[entity_identifier][f"{NAMESPACE_COMMON_MODEL}:receiverBundleId"]),
        #f"{NAMESPACE_COMMON_MODEL}:receiverServiceUri": bndl.valid_qualified_name(prov_req[entity_identifier][f"{NAMESPACE_COMMON_MODEL}:receiverServiceUri"]),
        f"{NAMESPACE_COMMON_MODEL}:metabundle": bndl.valid_qualified_name(prov_req[entity_identifier][f'{NAMESPACE_COMMON_MODEL}:metabundle'])
    })
    
    # Receiver Agent
    recAgent = bndl.agent(f"pathologyDepartment", other_attributes={
        f"{NAMESPACE_PROV}:type": bndl.valid_qualified_name(f"{NAMESPACE_COMMON_MODEL}:receiverAgent")
    })
    
    # Main Acitivity
    act_training = bndl.activity(f'training', other_attributes={
        f"{NAMESPACE_PROV}:type": bndl.valid_qualified_name(f"{NAMESPACE_COMMON_MODEL}:mainActivity"),
        f"{NAMESPACE_DCT}:hasPart": "biomaterialCollection"
    })
    
    bndl.wasGeneratedBy(conn_sample_connector, act_training)
    bndl.attribution(conn_sample_connector, recAgent)
    
    # --------------------------------------------------------------------------------------------------------
    data = parse_log(nis_log)
    
    ent_sample = bndl.entity(f'sample' ,other_attributes={
        "biopticAppId": f"{data['BiopticId']}"
    })
    bndl.specializationOf(ent_sample, conn_sample_connector)
    
    act_biocol = bndl.activity(f'biomaterialCollection', other_attributes={
        "startTime": f"{data['RequestDate']}",
        "endTime": f"{data['RequestDate']}"
    })
    bndl.used(ent_sample, act_biocol)
    
    ent_patient = bndl.entity(f'patient' ,other_attributes={
        "patientId": f"{data['PatientId']}"
    })
    bndl.used(act_biocol, ent_patient)
    bndl.wasDerivedFrom(ent_sample, ent_patient)
    
    # Doctor Agent
    doc_agent = bndl.agent(f"doctor", other_attributes={
        f"employeeId": f"{data['RequesterId']}",
        f"{NAMESPACE_PROV}:type": bndl.valid_qualified_name(f"{NAMESPACE_PROV}:Person"),
    })
    bndl.wasAssociatedWith(act_biocol, doc_agent)
    bndl.wasAttributedTo(ent_sample, doc_agent)
    
    return doc


def export(doc, nis_log):
    bndl = list(doc.bundles)[0]
    
    subdirs = ['', 'graph', 'json', 'provn', 'log']
    for subdir in subdirs:
        if not (OUTPUT_DIR / subdir).exists():
            (OUTPUT_DIR / subdir).mkdir(parents=True)

    export_to_image(bndl, (OUTPUT_DIR / 'graph' / BUNDLE_SURGERY_DEPARTMENT).with_suffix('.png'))
    export_to_file(doc, (OUTPUT_DIR / 'json' / BUNDLE_SURGERY_DEPARTMENT).with_suffix('.json'), format='json')
    export_to_file(doc, OUTPUT_DIR / 'provn' / BUNDLE_SURGERY_DEPARTMENT, format='provn')
    
    # Provenance of provenance
    output_log = {
        'git_commit_hash': str(pygit2.Repository('.').revparse_single('HEAD').hex),
        'script': str(__file__),
        'eid': str(uuid.uuid4()),
        'input': {
            'nis_log': str(nis_log.resolve()),
        },
        'output': {
            'local_png': str((OUTPUT_DIR / 'graph' / BUNDLE_SURGERY_DEPARTMENT).with_suffix('.png')),
            'remote_png': str(GRAPH_NAMESPACE_URI + str(Path(BUNDLE_SURGERY_DEPARTMENT).with_suffix('.png'))),
            'local_provn': str(OUTPUT_DIR / 'provn' / BUNDLE_SURGERY_DEPARTMENT),
            'remote_provn': str(PROVN_NAMESPACE_URI + BUNDLE_SURGERY_DEPARTMENT)
        }
    }
    with open(OUTPUT_DIR / 'log' / f'{BUNDLE_SURGERY_DEPARTMENT}.log', 'w') as json_out:
        json.dump(output_log, json_out, indent=3)

        
def attribute_values_to_qualnames(doc: prov.ProvDocument, entity: prov.ProvEntity):
    '''Attribute values are normally saved as strings. 
    For a namespace to transfer to new doc it must be
    saved as QualifiedName.'''
    for attr_name, attr_vals in entity._attributes.items():
        new_attr_vals = []
        for attr_val in attr_vals:
            new_attr_val = doc.valid_qualified_name(attr_val)
            new_attr_vals.append(new_attr_val)
        entity._attributes[attr_name] = set(new_attr_vals)
    return entity


if __name__=='__main__':
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)

    # Required arguments
    parser.add_argument('--nis_log', type=Path, required=True, help='Path to sample acquisition log exported from NIS')
    parser.add_argument('--requests', type=Path, required=False, help='Paths to optional requests from other organisations.')
    args = parser.parse_args()    
    
    doc = export_provenance(args.nis_log, args.requests)
    export(doc, args.nis_log)