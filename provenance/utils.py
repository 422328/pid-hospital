# Standard Imports
from datetime import datetime
from typing import Dict, List
from pathlib import Path
import secrets
import hashlib
import json

# Third-party Imports
from datacite import DataCiteRESTClient as client
from datacite import schema42 as schema
from datacite.errors import DataCiteError
import xml.etree.ElementTree as ET
import pandas as pd
import prov.model
import prov.dot
import pygit2
import os


# Local Imports


def parse_log(filepath: Path) -> Dict:
    # Open Provenance Log
    with open(filepath, 'r') as log_in:
        log = json.load(log_in)

    return log


def flatten_lists(cfg: Dict) -> Dict:
    for key, val in cfg.items():
        if isinstance(val, list):
            cfg[key] = flatten_list(val)
    return cfg


def flatten_list(l: List):
    if not isinstance(l, List):
        raise ValueError('Not a list')
    str_list = [str(x) for x in l]
    return ', '.join(str_list)


def flatten_dict(d: Dict, sep='_') -> Dict:
    """Flattens dictionary by concatenating the nested keys
       Source: https://stackoverflow.com/a/41801708/9734414    
    """
    return pd.json_normalize(d, sep=sep).to_dict(orient='records')[0]


def hash_tables_by_groups(filepath: Path, groups: List[str]) -> Dict:
    """Given a path to a pandas.HDFStore it calculates the sha256
    hash for each table as a combination of the table content and
    its associated metadata only.

    Args:
        filepath (Path): path to pandas.HDFStore
        groups (List[str]): list of group names

    Returns:
        Dict: dictionary of hashes grouped by groups
    """
    hdfs = pd.HDFStore(path=filepath, mode='r')

    G_tables = {}
    for g_name in groups:
        G_tables[g_name] = {}
        for idx, node in enumerate(hdfs.get_node(f'/{g_name}')):
            checksum = hash_table(hdfs, node._v_pathname)
            G_tables[g_name][f'table_{idx}_sha256'] = checksum

    hdfs.close()
    return G_tables


def hash_tables_by_keys(hdfs_handler: pd.HDFStore, table_keys: List[str]) -> Dict:
    """Given a path to a pandas.HDFStore it calculates the sha256
    hash for each table as a combination of the table content and
    its associated metadata only.

    Args:
        filepath (Path): path to pandas.HDFStore
        groups (List[str]): list of group names

    Returns:
        Dict: dictionary of hashes grouped by groups
    """
    result = {}
    for idx, table_key in enumerate(table_keys):
        checksum = hash_table(hdfs_handler, table_key)
        result[f'table_{idx}_sha256'] = checksum
    return result


def hash_table(hdfs_handler: pd.HDFStore, table_key: str) -> str:
    """Helper function for computing a hash for a single table.

    Args:
        hdfs_handler (pd.HDFStore): file handler to opened pd.HDFStore
        table_key (str): key for a table

    Returns:
        str: sha256 hash for a given table
    """
    sha256 = hashlib.sha256()

    # Table Checksum
    df = hdfs_handler.get(table_key)
    sha256.update(pd.util.hash_pandas_object(df).values)

    # Metadata Checksum
    d = hdfs_handler.get_storer(table_key).attrs.metadata
    [sha256.update(str.encode(repr(item), 'UTF-8')) for item in d.items()]

    checksum = sha256.hexdigest()
    return checksum
        

def get_hash(filepath: str, mock_env: bool = False, hash_type: str = 'sha256') -> str:
    """Generate a SHA256 hash of a file with a given filename.

    For the purpose of this thesis it can generate a random string when the given path is
    is not valid. If the script would be really deployed on server it would not be necessary.
    Adjusted example from:
    https://www.quickprogrammingtips.com/python/how-to-calculate-sha256-hash-of-a-file-in-python.html

    Args:
        filename: The path to the file whose hash is to be calculated.
        mock_env: Whether the provenance is to be generated in a simulated mocked environment.

    Returns:
        Calculated hash value of the file or a random hash if the file does not exist and the function
        was called in a simulated mocked environment.

    Raises:
        FileNotFoundError: If the file does not exist and the function was not called in a simulated
        mocked enviroment.
    """
    if filepath is None:
        return None

    filepath = Path(filepath)
    if hash_type == 'sha256':
        hash_m = hashlib.sha256()
    elif hash_type == 'md5':
        hash_m = hashlib.md5()
    else:
        raise ValueError(f'Invalid hash type: {hash_type}. Must be either sha256 or md5.')
    if not filepath.exists():
        if mock_env:
            # For Keras checkpoint files
            return secrets.token_urlsafe(32)
        else:
            raise ValueError('File does not exist')
    elif filepath.is_dir():
        for filepath_i in filepath.glob('*'):
            hash_m.update(bytes.fromhex(get_hash(filepath_i.resolve(), hash_type=hash_type)))
        return hash_m.hexdigest()
    elif filepath.is_file():
        with open(filepath, "rb") as f:
            # read and update hash string value in blocks of 4K
            for byte_block in iter(lambda: f.read(4096), b""):
                hash_m.update(byte_block)
            return hash_m.hexdigest()
    else:
        raise ValueError('Unknown file type.')


def export_to_image(bundle: prov.model.ProvBundle, filepath: Path) -> None:
    """Create a png image from a bundle and suffix it with given name.
    
    Args: 
        bundle: The bundle of which the provenance is to be exported.
        name: The name of the bundle as a string, given a suffix to the filename
              of the image.
    """
    # FIXME: Configure which output directory
    dot = prov.dot.prov_to_dot(bundle)
    dot.write_png(filepath)


def export_to_file(doc: prov.model.ProvDocument, filepath: Path, format: str) -> None:
    """Save bundle/document in a specified format in current directory"""
    with filepath.open('w') as f:
        doc.serialize(f, format=format)  


def export_to_datacite(
    organisation_doi: str,
    entity_identifier: str,
    remote_git_repo_path: str
):
    
    if 'DATACITE_REPOSITORY_USERNAME' not in os.environ:
        raise ValueError('DATACITE_REPOSITORY_USERNAME environmental variable not set.')
    
    if 'DATACITE_REPOSITORY_PASSWORD' not in os.environ:
        raise ValueError('DATACITE_REPOSITORY_PASSWORD environmental variable not set.')
    
    # Initialize the REST client.
    d = client(
        username=os.environ['DATACITE_REPOSITORY_USERNAME'],
        password=os.environ['DATACITE_REPOSITORY_PASSWORD'],
        prefix=organisation_doi,
        test_mode=False
    )
    
    metadata= {
        'identifiers': [{
            'identifierType': 'DOI',
            'identifier': entity_identifier,
        }],
        'creators': [
            {'name': 'Rudolf Wittner'},
            {'name': 'Matej Gallo'}
        ],
        'titles': [
            {'title': f'CPM Linking Paper {entity_identifier.split("/")[-1]}', }
        ],
        'publisher': 'Masaryk University',
        'publicationYear': '2023',
        'types': {
            'resourceType': 'Text',
            'resourceTypeGeneral': 'Text'
        },
        'schemaVersion': 'http://datacite.org/schema/kernel-4',
    }
    
    try:
        d.public_doi(metadata=metadata, url=remote_git_repo_path, doi=entity_identifier)
    except DataCiteError as e:
        d.update_doi(doi=entity_identifier, metadata=metadata, url=remote_git_repo_path)
    finally:
        d.show_doi(doi=entity_identifier)
        

